#include "led.h"
#include "timer_interrupts.h"
#include "keyboard.h"

void Automat(void){
	enum LedState{IDLE, LED_RIGHT, LED_LEFT};
	static enum LedState eLedState = LED_RIGHT;
		switch(eLedState){
			case IDLE:
				if(eKeyboardRead() == BUTTON_2) {
					eLedState = LED_RIGHT;
				}
				else if (eKeyboardRead() == BUTTON_0) {
					eLedState = LED_LEFT;
				}
				else{
					eLedState = IDLE;
				}
				break;
			case LED_LEFT:
				LedStepLeft();
				if(eKeyboardRead() == BUTTON_1){
					eLedState = IDLE;
				}
				else{
					eLedState = LED_LEFT;
				}
				break;			
			case LED_RIGHT:
				LedStepRight();
				if(eKeyboardRead() == BUTTON_1){
					eLedState = IDLE;
				}
				else{
					eLedState = LED_RIGHT;
				}
				break;
		}
}

int main(){
	unsigned int iMainLoopCtr;
	LedInit();
	KeyboardInit();
	Timer0Interrupts_Init(20000,&Automat);
	
	while(1){
		iMainLoopCtr++;
			iMainLoopCtr++;	
		iMainLoopCtr++;
			iMainLoopCtr++;
	}
}
